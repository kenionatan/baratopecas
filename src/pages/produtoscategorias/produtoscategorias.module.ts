import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProdutoscategoriasPage } from './produtoscategorias';

@NgModule({
  declarations: [
    ProdutoscategoriasPage,
  ],
  imports: [
    IonicPageModule.forChild(ProdutoscategoriasPage),
  ],
})
export class ProdutoscategoriasPageModule {}
