import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { MarcaPage } from '../pages/marca/marca';
import { ProdutoscategoriasPage } from '../pages/produtoscategorias/produtoscategorias';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CategoriasPage } from '../pages/categorias/categorias';
import { ServiceProvider } from '../providers/service/service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule} from '@angular/http';
import { BuscaPage } from '../pages/busca/busca';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    CategoriasPage,
    MarcaPage,
    ProdutoscategoriasPage,
    BuscaPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    CategoriasPage,
    MarcaPage,
    ProdutoscategoriasPage,
    BuscaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServiceProvider
  ]
})
export class AppModule {}
